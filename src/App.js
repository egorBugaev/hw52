import React, { Component } from 'react';
import './App.css';
import check, {GetRandom ,numbers } from "./numbers.js";



class App extends Component {
    state = {
        numbers: numbers,

    };


    getRandomNumbers = () => {
        let number = check(GetRandom(5, 33));
        this.setState({number});

    };



    render() {
    return (
      <div className="App">
         <button onClick={this.getRandomNumbers}>Get new numbers</button>
         <div className="cont">
            <div id="first">
                <h3>{this.state.numbers.first}</h3>
            </div>
            <div id="second">
                <h3>{this.state.numbers.second}</h3>
            </div>
            <div id="third">
                <h3>{this.state.numbers.third}</h3>
            </div>
            <div id="fourth">
                <h3>{this.state.numbers.fourth}</h3>
            </div>
            <div id="fifth">
                <h3>{this.state.numbers.fifth}</h3>
            </div>

        </div>
      </div>
    );
  }
}
// document.getElementById("numbers").onclick = function () {
//     check(GetRandom(5, 33));
//     ss();
// };
export default App;



